FROM java:8
ADD target/eurekk-server-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 1111
ENTRYPOINT ["java","-Xmx1G","-jar","/app.jar","--logging.level.test=OFF"]
